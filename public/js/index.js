// mobile menu
const burgerIcon = document.querySelector('#burger');
const navbarMenu = document.querySelector('#nav-links');

burgerIcon.addEventListener('click', () => {
    navbarMenu.classList.toggle('is-active');
})

// modal - pop up
const signupButton = document.querySelector('#bookNow');
const modalBg = document.querySelector('.modal-background');
const modal = document.querySelector('.modal');

// show the message
signupButton.addEventListener('click', () => {
    modal.classList.add('is-active');
})

// hide the message
modalBg.addEventListener('click', () => {
    modal.classList.remove('is-active');
})

